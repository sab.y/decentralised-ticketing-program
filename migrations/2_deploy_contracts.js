const TicketingProgram = artifacts.require("TicketingProgram");
const AddressVerification = artifacts.require("AddressVerification");

module.exports = function (deployer) {
  deployer.deploy(AddressVerification).then((addressVerificationObject)=>{
    const addressVerificationAddress = addressVerificationObject.address;
    return deployer.deploy(TicketingProgram, addressVerificationAddress);
  });
};
