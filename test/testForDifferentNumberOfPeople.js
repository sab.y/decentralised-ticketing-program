  const Web3 = require("web3");
const TicketingProgram = artifacts.require("TicketingProgram");
const AddressVerification = artifacts.require("AddressVerification");

contract("Ticketing", (accounts) => {
  async function runTest(accounts, numUsers, numRuns) {
    let totalDuration = 0;
    let totalTps = 0;
  
    contract(`${numUsers} users`, () => {
      for (let run = 0; run < numRuns; run++) {
        let ticketingSystem;
        let addressVerification;
        const users = accounts.slice(1, numUsers + 1);
  
        before(async () => {
          addressVerification = await AddressVerification.new();
          ticketingSystem = await TicketingProgram.new(addressVerification.address);
        });
  
        it(`Simulates ticket transfers for ${numUsers} users )`, async () => {
          const eventId = 1;
          const totalTickets = 1000;
          await ticketingSystem.createEvent("Large Event", 31231, "London", Web3.utils.toWei("1", "ether"), totalTickets, true, true);
          for (const user of users) {
            await addressVerification.verifyUser(user);
          }
          
          const startTime = Date.now();
          
          for (let i = 0; i < users.length; i++) {
            await ticketingSystem.buyTicket(eventId, { from: users[i], value: Web3.utils.toWei("1", "ether") });
          }
          for (let i = 0; i < users.length - 1; i++) {
            const ticketId = i + 1;
            await ticketingSystem.sellTicket(ticketId, Web3.utils.toWei("1", "ether"), { from: users[i] });
          }

          const endTime = Date.now();
          const duration = (endTime - startTime) / 1000;
          const tps = ((users.length - 1) * 2) / duration;
  
          totalDuration += duration;
          totalTps += tps;
        });
      }

      after(() => {
        const avgDuration = totalDuration / numRuns;
        const avgTps = totalTps / numRuns;
    
        console.log(`Average duration for ${numUsers} users: ${avgDuration.toFixed(2)} seconds`);
        console.log(`Average transactions per second for ${numUsers} users: ${avgTps.toFixed(2)} TPS`);
      });
    });
  }

  it("Simulates ticket transfers for varying number of users", async () => {
    const numRuns = 10;
    await runTest(accounts, 200, numRuns);

  });
});
