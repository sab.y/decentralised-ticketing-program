const Web3 = require("web3");
const AddressVerification = artifacts.require("AddressVerification");
const TicketingProgram = artifacts.require("TicketingProgram");

contract("Scalability Test", (accounts) => {
  let ticketingSystem;
  let addressVerification;
  const users = accounts.slice(1, 50); // 100 accounts to test

  before(async () => {
    addressVerification = await AddressVerification.new();
    ticketingSystem = await TicketingProgram.new(addressVerification.address);
  });

  it("Simulates buying and selling tickets two users with additional load", async () => {
    const eventId = 1;
    const totalTickets = 100;
    const user1 = users[0];
    const user2 = users[1];
    const ticketPromises = [];
    const sellPromises = [];


    await ticketingSystem.createEvent("Large Event", 3123321, "New York", Web3.utils.toWei("1", "ether"), totalTickets, true, true);
    await addressVerification.verifyUser(user1);
    await addressVerification.verifyUser(user2);

    const start = Date.now();

    for (let i = 0; i < 20; i++) {
      ticketPromises.push(
        ticketingSystem.buyTicket(eventId, { from: user1, value: Web3.utils.toWei("1", "ether") })
      );
    }
    await Promise.all(ticketPromises);

    for (let i = 1; i <= 20; i++) {
        sellPromises.push(ticketingSystem.sellTicket(i, Web3.utils.toWei("1", "ether"), { from: user1 }));
      }
  


    for (let i = 0; i <= 2000; i++){
    await ticketingSystem.getTicket(i)
    }


    // Wait for all ticket purchase transactions to be mined.
    await Promise.all(sellPromises);

    // Now user2 buys all tickets from user1.
    for (let i = 0; i < 20; i++) {
      await ticketingSystem.buyUsedTicket(eventId, { from: user2, value: Web3.utils.toWei("1", "ether") });
    }
   

    const end = Date.now();

    const duration = (end - start) / 1000;
    console.log(`Duration: ${duration.toFixed(2)} seconds`);

    

    assert.ok(true, "Scalability test completed");
  });
});
