const TicketingProgram = artifacts.require("TicketingProgram");
const AddressVerification = artifacts.require("AddressVerification");

contract("Gas Usage Test", (accounts) => {
  let ticketingProgram;
  let addressVerification;

  const gasPrices = [30, 50, 70, 90].map(price => web3.utils.toWei(price.toString(), 'gwei'));

  beforeEach(async () => {
    addressVerification = await AddressVerification.new();
    ticketingProgram = await TicketingProgram.new(addressVerification.address);
    await addressVerification.verifyUser(accounts[1]);
    await addressVerification.verifyUser(accounts[2]);
    await ticketingProgram.createEvent("Test Event", 16719289000, "Test Location", 1, 150, true, true);
  });

  for (const gasPrice of gasPrices) {
    it(`gas usage for createEvent at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei`, async () => {
      const tx = await ticketingProgram.createEvent("Library session", 321312, "London", 1, 150, true, true, { from: accounts[0], gasPrice });
      console.log(`Gas used for createEvent at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei:`, tx.receipt.gasUsed);
    });

    it(`gas usage for buyTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei`, async () => {
      const tx = await ticketingProgram.buyTicket(1, { from: accounts[1], value: 1, gasPrice });
      console.log(`Gas used for buyTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei:`, tx.receipt.gasUsed);
    });

    it(`gas usage for sellTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei`, async () => {
      await ticketingProgram.buyTicket(1, { from: accounts[1], value: 1, gasPrice });
      const tx = await ticketingProgram.sellTicket(1, 2, { from: accounts[1], gasPrice });
      console.log(`Gas used for sellTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei:`, tx.receipt.gasUsed);
    });

    it(`gas usage for buyUsedTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei`, async () => {
      await ticketingProgram.buyTicket(1, { from: accounts[1], value: 1, gasPrice });
      await ticketingProgram.sellTicket(1, 2, { from: accounts[1], gasPrice });
      const tx = await ticketingProgram.buyUsedTicket(1, { from: accounts[2], value: 2, gasPrice });
      console.log(`Gas used for buyUsedTicket at ${web3.utils.fromWei(gasPrice, 'gwei')} Gwei:`, tx.receipt.gasUsed);
    });
  }
});
