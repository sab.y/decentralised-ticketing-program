const Web3 = require("web3");
const TicketingProgram = artifacts.require("TicketingProgram");
const AddressVerification = artifacts.require("AddressVerification");

contract("", (accounts) => {
  let ticketingSystem;
  let addressVerification;
  const users = accounts.slice(1, 351);

  before(async () => {
    addressVerification = await AddressVerification.new();
    ticketingSystem = await TicketingProgram.new(addressVerification.address);
  });

  it("Simulates ticket transfers for 350 users and measures latency", async () => {
    const eventId = 1;
    const totalTickets = 10000;
    await ticketingSystem.createEvent("Dissertation", 7646543, "Leeds", Web3.utils.toWei("1", "ether"), totalTickets, true, true);
    
    for (const user of users) {
      await addressVerification.verifyUser(user);
    }

    let buyTicketLatencyTimes = [];
    let sellTicketLatencyTimes = [];
    let buyUsedTicketLatencyTimes = [];


    for (let i = 0; i < users.length; i++) {
      let buyTicketStartTime = Date.now();
      await ticketingSystem.buyTicket(eventId, { from: users[i], value: Web3.utils.toWei("1", "ether") });
      let buyTicketEndTime = Date.now();
      let buyTicketLatency = buyTicketEndTime - buyTicketStartTime;
      // Add this latency to the array
      buyTicketLatencyTimes.push(buyTicketLatency);

      if (i < users.length - 1) {  // No ticket to sell for the last user
        let sellTicketStartTime = Date.now();
        await ticketingSystem.sellTicket(i + 1, Web3.utils.toWei("1", "ether"), { from: users[i] });
        let sellTicketEndTime = Date.now();
        let sellTicketLatency = sellTicketEndTime - sellTicketStartTime;
        sellTicketLatencyTimes.push(sellTicketLatency);
        let buyUsedTicketStartTime = Date.now();
        await ticketingSystem.buyUsedTicket(eventId, { from: users[i + 1], value: Web3.utils.toWei("1", "ether") });
        let buyUsedTicketEndTime = Date.now();
        let buyUsedTicketLatency = buyUsedTicketEndTime - buyUsedTicketStartTime;
        buyUsedTicketLatencyTimes.push(buyUsedTicketLatency);
      }
    }

    // Print average latency times
    let avgBuyTicketLatency = buyTicketLatencyTimes.reduce((a, b) => a + b, 0) / buyTicketLatencyTimes.length;
    let avgSellTicketLatency = sellTicketLatencyTimes.reduce((a, b) => a + b, 0) / sellTicketLatencyTimes.length;
    let avgBuyUsedTicketLatency = buyUsedTicketLatencyTimes.reduce((a, b) => a + b, 0) / buyUsedTicketLatencyTimes.length;

    console.log(`Average buyUsedTicket latency: ${avgBuyUsedTicketLatency.toFixed(2)} ms`);
    console.log(`Average sellTicket latency: ${avgSellTicketLatency.toFixed(2)} ms`);
    console.log(`Average buyTicket latency: ${avgBuyTicketLatency.toFixed(2)} ms`);



    assert.ok(true, "Latency test completed"); 
  });
});
