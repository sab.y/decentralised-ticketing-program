const Web3 = require("web3");
const TicketingProgram = artifacts.require("TicketingProgram");
const AddressVerification = artifacts.require("AddressVerification");

contract("", (accounts) => {
  let ticketingSystem;
  let addressVerification;
  const users = accounts.slice(1, 351); 

  before(async () => {
    addressVerification = await AddressVerification.new();
    ticketingSystem = await TicketingProgram.new(addressVerification.address);
  });

  it("Worst case simulation", async () => {
    // Create a large event with 150 tickets
    const eventId = 1;
    const totalTickets = 10000;
    await ticketingSystem.createEvent("Large Event", 848138, "London", Web3.utils.toWei("1", "ether"), totalTickets, true, true);
    const startTime = Date.now();
    for (const user of users) {
      await addressVerification.verifyUser(user);
    }
    // Buy tickets for all users
    for (let i = 0; i < users.length; i++) {
      await ticketingSystem.buyTicket(eventId, { from: users[i], value: Web3.utils.toWei("1", "ether") });
    }


    // Transfer tickets among users
    for (let i = 0; i < users.length - 1; i++) {
      const ticketId = i + 1;
      await ticketingSystem.sellTicket(ticketId, Web3.utils.toWei("1", "ether"), { from: users[i] });
      await ticketingSystem.buyUsedTicket(eventId, { from: users[i + 1], value: Web3.utils.toWei("1", "ether") });
    }

    const endTime = Date.now();
    const duration = (endTime - startTime) / 1000;
    const tps = (users.length + (users.length - 1) * 2) / duration;

    console.log(`Duration: ${duration.toFixed(2)} seconds`);
    console.log(`Transactions per second: ${tps.toFixed(2)} TPS`);

     });
});
