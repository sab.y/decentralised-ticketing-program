const HDWalletProvider = require('@truffle/hdwallet-provider');
const mnemonic = "legal honey sock shy cloth earn various island foil vanish wasp choose";

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
    },
    test: {
      provider: () => new HDWalletProvider(mnemonic, "http://127.0.0.1:7545"),
      network_id: "*",
      gas: 6721975,
      gasPrice: 20000000000, // 20 gwei
    },
  },

  mocha: {
    // timeout: 100000
  },

  compilers: {
    solc: {
      version: "0.8.19",
    },
  },
};
