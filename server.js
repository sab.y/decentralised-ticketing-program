const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Web3 = require('web3');
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();

const app = express();

const web3 = new Web3('http://127.0.0.1:7545'); 



const ticketingProgramABI = JSON.parse(fs.readFileSync('./build/contracts/TicketingProgram.json', 'utf8')).abi;
const ticketingProgramAddress = '0x9c77cf23FFCFC116daA0dbBc3ACd497767DA46A1';
const ticketingProgram = new web3.eth.Contract(ticketingProgramABI, ticketingProgramAddress);

const addressVerificationABI=  JSON.parse(fs.readFileSync('./build/contracts/AddressVerification.json', 'utf8')).abi;
const addressVerificationAddress= '0xc72Ee3e37cF4A3d8b5F81fe5EFcf312017bC49c2';
const addressVerification = new web3.eth.Contract(addressVerificationABI,addressVerificationAddress);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


const db = new sqlite3.Database('./users.db',(err)=>{
  if (err){
    console.log("\n ******** Error:  ******\n");
    console.log(err.message);
  }
  else{
    console.log("Connected to DB");
  }
}
);

db.run('CREATE TABLE users (address TEXT PRIMARY KEY, firstName TEXT, lastName TEXT, postcode TEXT, licenseNumber TEXT)', (err) => {
  if (err) {
    console.log("\n ******** Error:  ******\n");
    console.log(err.message);
  }
  console.log('Succesfull database creation');
});



app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});



app.post('/createEvent', async (req, res) => {
  try {
    const name = req.body.name;
    const date = req.body.date;
    const location = req.body.location;
    const ticketPrice = req.body.ticketPrice;
    const totalTickets = req.body.totalTickets;
    const isResaleAllowed = req.body.isResaleAllowed;
    const isPriceIncreaseAllowed = req.body.isPriceIncreaseAllowed;

    const privateKey = '0xa5025f798e507342e52aa99ef1cd34afa2cadc559e9a2e5c080392413d820837';
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    web3.eth.accounts.wallet.add(account);

    const gas = await ticketingProgram.methods.createEvent(name, date, location, ticketPrice, totalTickets, isResaleAllowed, isPriceIncreaseAllowed).estimateGas({ from: account.address });
    const nonce = await web3.eth.getTransactionCount(account.address);

    const transaction = {
      to: ticketingProgramAddress,
      data: ticketingProgram.methods.createEvent(name, date, location, ticketPrice, totalTickets, isResaleAllowed, isPriceIncreaseAllowed).encodeABI(),
      gas: gas,
      nonce: nonce,
    };

    const signedTx = await web3.eth.accounts.signTransaction(transaction, privateKey);
    const txReceipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);

    console.log('Event created successfully:', txReceipt);

    res.send({ message: 'Event created successfully' });
  } catch (error) {
    console.log('Error creating event:', error);
    res.status(500).send({ error: 'Error creating event' });
  }
});

app.get('/events', (req, res) => {
  res.sendFile(__dirname + '/events.html');
});

app.get('/fetchEvents', async (req, res) => {
  try {
    const eventsList = [];
    const eventId = await ticketingProgram.methods.eventId().call();

    for (let i = 1; i <= eventId; i++) {
      const event = await ticketingProgram.methods.events(i).call();
      eventsList.push(event);
    }

    res.send(eventsList);
  } catch (error) {
    console.log('Error fetching events:', error);
    res.status(500).send({ error: 'Error fetching events' });
  }
});

app.get('/getTicket/:ticketId', async (req,res)=>{
  try{
    const ticketID = req.params.ticketId;
    const ticket = await ticketingProgram.methods.getTicket(ticketID).call();

    res.send({
      id:ticket[0],
      holder:ticket[1],
      price:ticket[2],
      eventId: ticket[3],
    });
  }catch (error){
    console.error("****Error getting ticket ********", error);
    res.status.send({error: "ticket details error"});
  }
})

app.get('/verification', (req, res) => {
  res.sendFile(__dirname + '/verification.html');
});

app.post('/verify', async (req, res) => {
  const { address, firstName, lastName, postcode, licenseNumber } = req.body;
  
  try {
    await db.run('INSERT INTO users (address, firstName, lastName, postcode, licenseNumber) VALUES (?, ?, ?, ?, ?)', [address, firstName, lastName, postcode, licenseNumber]);

    const privateKey = '0xa5025f798e507342e52aa99ef1cd34afa2cadc559e9a2e5c080392413d820837';
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    const fromAccount = account.address;
    const gasPrice = await web3.eth.getGasPrice();
    const gasLimit = await addressVerification.methods.verifyUser(address).estimateGas({ from: fromAccount });

    const transaction = addressVerification.methods.verifyUser(address).send({ from: fromAccount, gasPrice, gasLimit });

    // Wait for tx to be mined
    const receipt = await transaction;
    console.log('Transaction receipt:', receipt);

    console.log('ADDED USER:', address, firstName, lastName, postcode, licenseNumber);
    res.json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: 'Unable to verify user address.' });
  }
});




app.post('/purchaseTicket', async (req, res) => {
  try {
    const eventId = req.body.eventId;
    const privateKey = req.body.privateKey;

    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    web3.eth.accounts.wallet.add(account);

    //if doesnt return fulfilled when calling eventID, event does not exist
    const event = await ticketingProgram.methods.events(eventId).call();
    const ticketPrice = event.ticketPrice;
    
    const gas = await ticketingProgram.methods.buyTicket(eventId).estimateGas({ from: account.address, value: ticketPrice });
    const nonce = await web3.eth.getTransactionCount(account.address);

    const transaction = {
      to: ticketingProgramAddress,
      data: ticketingProgram.methods.buyTicket(eventId).encodeABI(),
      gas: gas,
      nonce: nonce,
      value: ticketPrice,
      from: account.address, 
    };

    const signedTx = await web3.eth.accounts.signTransaction(transaction, privateKey);
    const txReceipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);

    console.log('Ticket purchased successfully:', txReceipt);

    res.send({ message: 'Ticket purchased successfully' });
  } catch (error) {
    console.log('Error purchasing ticket:', error);
    res.status(500).send({ error: 'Error purchasing ticket' });
  }
});


app.post('/sellTicket', async (req, res) => {
  const { userAddress, privateKey, ticketId, price } = req.body;

  try {
    const tx = {
      from: userAddress,
      to: ticketingProgramAddress,
      gas: 1000000,
      data: ticketingProgram.methods.sellTicket(ticketId, price).encodeABI()
    };

    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);
    const txReceipt = await web3.eth.sendSignedTransaction(signedTx);

    res.send({ success: true, result: txReceipt });
  } catch (error) {
    console.error("Error selling ticket:", error);
    res.status(500).send({ error: "Error selling ticket" });
  }
});

app.get("/sellATicket",(req,res)=>{
  res.sendFile(__dirname + '/sellTicket.html');
  
});



app.post('/buyUsedTicket', async (req, res) => {
  const { eventId, price, account } = req.body;

  try {
    const tx = {
      from: account,
      to: ticketingProgram.options.address,
      gas: 1000000,
      value: web3.utils.toWei(price, 'wei'),
      data: ticketingProgram.methods.buyUsedTicket(eventId).encodeABI(),
    };

    const txReceipt = await web3.eth.sendTransaction(tx);
    res.send({ success: true, result: txReceipt });
  } catch (error) {
    console.error("Error buying ticket:", error);
    res.status(500).send({ error: "Error buying ticket" });
  }
});

app.get("/usedTickets",(req,res)=>{
  res.sendFile(__dirname + '/buyUsedTicket.html');
});


app.get('/getUsedTickets/:eventId', async (req, res) => {
  const eventId = req.params.eventId;
  const usedTickets = [];

  try {
    const totalTickets = await ticketingProgram.methods.ticketId().call();
    for (let i = 1; i <= totalTickets; i++) {
      const ticket = await ticketingProgram.methods.tickets(i).call();
      if (ticket.eventId == eventId && ticket.holder == '0x0000000000000000000000000000000000000000' && ticket.price != 0) {
        usedTickets.push(ticket);
      }
    }
    res.send(usedTickets);
  } catch (error) {
    console.error("Error fetching tickets:", error);
    res.status(500).send({ error: "Error fetching used tickets" });
  }
});



const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
