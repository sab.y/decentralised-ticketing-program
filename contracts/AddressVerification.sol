// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.18.0;

contract AddressVerification{
    mapping (address => bool) public verifiedUsers;

    function verifyUser(address _user) public{
        require (!verifiedUsers[_user], "This address is already verified");
        verifiedUsers[_user] = true;
    }

    function checkUserVerified(address _user) public view returns (bool){
        return verifiedUsers[_user];
    }
    
}