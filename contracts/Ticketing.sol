// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.18.0;

import "./AddressVerification.sol";

contract TicketingProgram  {
    
  struct Ticket {
    uint256 id;
    address holder;
    uint256 price;
    uint256 eventId;
  }

  struct Event {
    uint256 id;
    string name;
    uint256 date;
    string location;
    uint256 ticketPrice;
    uint256 totalTickets;
    uint256 soldTickets;
    bool isResaleAllowed;
    bool isPriceIncreaseAllowed;
  }
  mapping(uint256 => Ticket) public tickets;
  mapping(uint256 => Event) public events;
  uint256 public ticketId;
  uint256 public eventId;

  AddressVerification private verifyContract;

  constructor(address _addressVerificationContractAddress){
    verifyContract = AddressVerification(_addressVerificationContractAddress);
      }


  modifier verifiedUser(){
    require(verifyContract.checkUserVerified(msg.sender),"This address is not verified");
    _;
  }

  function createEvent(string memory _name, uint256 _date, string memory _location, uint256 _ticketPrice, uint256 _totalTickets,bool _isResaleAllowed, bool _isPriceIncreaseAllowed) public {
    eventId++;
    events[eventId] = Event(eventId, _name, _date, _location, _ticketPrice, _totalTickets, 0,_isResaleAllowed,_isPriceIncreaseAllowed);
  }

  event TicketPurchased(uint256 indexed ticketId, address indexed holder, uint256 price, uint256 indexed eventId);


function buyTicket(uint256 _eventId) public payable verifiedUser  {
    Event storage myEvent = events[_eventId];
    require(myEvent.soldTickets < myEvent.totalTickets, "All tickets are sold out!");
    require(msg.value == myEvent.ticketPrice, "Incorrect ticket price!");
    ticketId++;
    myEvent.soldTickets++;
    tickets[ticketId] = Ticket(ticketId, msg.sender, myEvent.ticketPrice, _eventId);

    emit TicketPurchased(ticketId, msg.sender, myEvent.ticketPrice, _eventId);
}

  function getTicket(uint256 _ticketId) public view returns (uint256 ticketID, address, uint256 price, uint256 eventID) {
    Ticket memory ticket = tickets[_ticketId];
    return (ticket.id, ticket.holder, ticket.price, ticket.eventId);
  }

function sellTicket(uint256 _ticketId, uint256 _price) public  verifiedUser{
  Ticket storage myTicket = tickets[_ticketId];
  uint256 eventID = myTicket.eventId;
  require(msg.sender == myTicket.holder, "You are not the ticket holder!");
  require(events[eventID].isResaleAllowed, "Ticket resale is not allowed for this event.");
  if(!(events[eventID].isPriceIncreaseAllowed)){
  require(_price <= myTicket.price, "You are not allowed to sell your ticket for more than you bought it in this event. ");
  }
  myTicket.holder = address(0);
  myTicket.price = _price;
}


function buyUsedTicket(uint256 _eventId) public payable verifiedUser  {
  Event storage myEvent = events[_eventId];
  require(myEvent.soldTickets < myEvent.totalTickets, "All tickets are sold out!");
  uint256 chosenIndex;
  for (uint256 i = 1; i <= ticketId; i++) {
    if (tickets[i].eventId == _eventId && tickets[i].holder == address(0) && tickets[i].price != 0) {
      chosenIndex  = i;
      break;
    }
  }
  require(msg.value == tickets[chosenIndex].price, "Incorrect ticket price!");
  require(tickets[chosenIndex].holder == address(0), "No tickets are available for this event!");
  
  address seller = tickets[chosenIndex].holder;
  tickets[chosenIndex].holder = msg.sender;
  myEvent.soldTickets++;
  payable(seller).transfer(msg.value);
}



function getResellTickets() public view returns (Ticket[] memory) {
  Ticket[] memory resellTickets = new Ticket[](ticketId);
  uint256 resellTicketCount = 0;
  for (uint256 i = 1; i <= ticketId; i++) {
    if (tickets[i].holder == address(0) && tickets[i].price != 0) {
      resellTickets[resellTicketCount] = tickets[i];
      resellTicketCount++;
    }
  }
  Ticket[] memory result = new Ticket[](resellTicketCount);
  for (uint256 i = 0; i < resellTicketCount; i++) {
    result[i] = resellTickets[i];
  }
  return result;
}


function getUserTicketIds(address user) public view returns (uint256[] memory) {
    uint256 ticketCount = 0;
    for (uint256 i = 1; i <= ticketId; i++) {
        if (tickets[i].holder == user) {
            ticketCount++;
        }
    }

    uint256[] memory userTickets = new uint256[](ticketCount);
    uint256 currentIndex = 0;
    for (uint256 i = 1; i <= ticketId; i++) {
        if (tickets[i].holder == user) {
            userTickets[currentIndex] = i;
            currentIndex++;
        }
    }

    return userTickets;
}




}
