async function fetchEvents() {
    const response = await fetch('/fetchEvents');
    const events = await response.json();
  
    const tableBody = document.querySelector('#eventsTable tbody');
    events.forEach((event) => {
      const row = document.createElement('tr');
  
      const idCell = document.createElement('td');
      idCell.textContent = event.id;
      row.appendChild(idCell);
  
      const nameCell = document.createElement('td');
      nameCell.textContent = event.name;
      row.appendChild(nameCell);
  
      const dateCell = document.createElement('td');
      dateCell.textContent = new Date(event.date * 1000).toLocaleString();
      row.appendChild(dateCell);
  
      const locationCell = document.createElement('td');
      locationCell.textContent = event.location;
      row.appendChild(locationCell);
  
      const ticketPriceCell = document.createElement('td');
      ticketPriceCell.textContent = event.ticketPrice;
      row.appendChild(ticketPriceCell);
  
      const totalTicketsCell = document.createElement('td');
      totalTicketsCell.textContent = event.totalTickets;
      row.appendChild(totalTicketsCell);
  
      const soldTicketsCell = document.createElement('td');
      soldTicketsCell.textContent = event.soldTickets;
      row.appendChild(soldTicketsCell);
  
      const actionCell = document.createElement('td');
      const buyButton = document.createElement('button');
      buyButton.textContent = 'Buy Ticket';
      actionCell.appendChild(buyButton);
      row.appendChild(actionCell);
  
      tableBody.appendChild(row);
    });
  }
  
  fetchEvents();
  